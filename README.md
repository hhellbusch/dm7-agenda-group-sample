This is a sample project for decision manager 7 that show the functionality of agenda-groups.

API Call Examples:

```json
{
  "lookup" : null,
  "commands" : [
    {
      "insert" : {
        "disconnected" : false,
        "out-identifier" : "input",
        "return-object" : true,
        "entry-point" : "DEFAULT",

        "object" : {
          "com.first_american_two.agendas.Person" : {
            "name":"Henry",
             "message": {"com.first_american_two.agendas.Message":{"message":"says hello!!"}}
          }
        }
      }
    },
    {"set-focus":{"name":"goodbye"}},
    {"set-focus":{"name":"hello"}},
    {"set-focus":{"name":"nicknames"}},

    {
      "fire-all-rules" : { }
    },
    {
      "get-objects":{
        "out-identifier":"output"
      }
    },
    {"dispose":{}}
  ]
}
```

Example Response:
```json
{
  "type" : "SUCCESS",
  "msg" : "Container agendas successfully called.",
  "result" : {
    "execution-results" : {
      "results" : [ {
        "value" : [{"com.first_american_two.agendas.Person":{
  "name" : "Hank",
  "message" : {
    "message" : "says hello!!"
  },
  "hair" : "bald"
}},{"com.first_american_two.agendas.Message":{
  "message" : "so long and thanks for all the fish"
}},{"com.first_american_two.agendas.Message":{
  "message" : "Goodbye Hank"
}},{"com.first_american_two.agendas.Message":{
  "message" : "Hello Hank"
}}],
        "key" : "output"
      }, {
        "value" : {"com.first_american_two.agendas.Person":{
  "name" : "Hank",
  "message" : {
    "message" : "says hello!!"
  },
  "hair" : "bald"
}},
        "key" : "input"
      } ],
      "facts" : [ {
        "value" : {"org.drools.core.common.DefaultFactHandle":{
  "external-form" : "0:1:746519127:746519127:2:DEFAULT:NON_TRAIT:com.first_american_two.agendas.Person"
}},
        "key" : "input"
      } ]
    }
  }
}
```

And example logs - 

```
2018-08-17 23:40:25,637 INFO  [stdout] (default task-17) Found a Henry! They are now Hank getName(): Hank
2018-08-17 23:40:25,638 INFO  [stdout] (default task-17) hello called for Hank
2018-08-17 23:40:25,639 INFO  [stdout] (default task-17) goodbye called Hank
2018-08-17 23:40:25,640 INFO  [stdout] (default task-17) goodbye hank called for Hank
2018-08-17 23:40:25,640 INFO  [stdout] (default task-17) set Hank to be bald
```

